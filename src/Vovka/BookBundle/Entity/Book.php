<?php
namespace Vovka\BookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="Vovka\BookBundle\Repository\BookRepository")
 * @ORM\Table(name="books")
 */
class Book
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="Author", inversedBy="books")
     * @ORM\JoinColumn(name="author", referencedColumnName="id")
     */
    protected $author;

    /**
     * @ORM\ManyToOne(targetEntity="PublishingHouse", inversedBy="books")
     * @ORM\JoinColumn(name="publishingHouse", referencedColumnName="id")
     */
    protected $publishingHouse;

    /**
     * @ORM\Column(type="date", options={"default" = NULL})
     */
    protected $dateRelease;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $path;

    /**
     * @Assert\File(maxSize="6000000")
     */
    protected $photo;

    public function setPhoto(UploadedFile $file = null)
    {
        $this->photo = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getPhoto()
    {
        return $this->photo;
    }


    public function upload()
    {

        if (null === $this->getPhoto()) {
            return;
        }

        $this->getPhoto()->move(
            $this->getUploadRootDir(),
            $this->getPhoto()->getClientOriginalName()
        );


        $this->path = $this->getPhoto()->getClientOriginalName();

        $this->photo = null;
    }


    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir() . '/' . $this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir() . '/' . $this->path;
    }

    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'photo';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return Book
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Book
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set author
     *
     * @param integer $author
     * @return Book
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return integer
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set publishingHouse
     *
     * @param integer $publishingHouse
     * @return Book
     */
    public function setPublishingHouse($publishingHouse)
    {
        $this->publishingHouse = $publishingHouse;

        return $this;
    }

    /**
     * Get publishingHouse
     *
     * @return integer
     */
    public function getPublishingHouse()
    {
        return $this->publishingHouse;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Book
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Set dateRelease
     *
     * @param \DateTime $dateRelease
     * @return Book
     */
    public function setDateRelease($dateRelease)
    {
        $this->dateRelease = $dateRelease;

        return $this;
    }

    /**
     * Get dateRelease
     *
     * @return \DateTime
     */
    public function getDateRelease()
    {
        return $this->dateRelease;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Book
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }
}