<?php

namespace Vovka\BookBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Vovka\BookBundle\Entity\Book;
use Vovka\BookBundle\Entity\Author;
use Vovka\BookBundle\Entity\PublishingHouse;
use Vovka\BookBundle\Form\Type\BookType;
use Vovka\BookBundle\Form\Type\FilterBookType;

/* Описание
 * Привет, если ты это читаешь, то ты наверное проверяешь мой код
 * Первый раз пишу на symfony, разбирался на ходу,
 * и наверняка что-то сделал не так.
 * Прошу не суди строго, хочу попасть к ребятам в aheadstudio :)
 */

class DefaultController extends Controller
{
    // Список книг + фильтр по всем полям через кастомный Репозиторий
    public function indexAction(Request $request)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('Vovka\BookBundle\Entity\Book');

        $filter = $request->query->all();

        $result = array();

        $params = array();

        if (isset($filter['book']['filter'])) {
            $params = $filter['book'];
            $resource = $repo->findByParams($params);
        } else
            $resource = $repo->findAll();

        $form = $this->createForm(new FilterBookType($params));

        $form->handleRequest($request);

        if (isset($filter['book']) && array_key_exists('reset', $filter['book']))
            return $this->redirect($this->get('router')->generate('index'));

        foreach ($resource as $book)
            $result[] = $this->extractFieds($book);

        return $this->render('BookBundle:Default:index.html.twig',
            array('books' => $result, 'form' => $form->createView()));
    }

    // Добавление новой книги
    public function newAction(Request $request)
    {

        $book = new Book();

        $form = $this->createForm(new BookType(), $book);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $book = $form->getData();
                $em = $this->getDoctrine()->getManager();

                $book->upload(); // Загрузка прикрепленного файла из временной директории в web/photo

                $em->persist($book);
                $em->flush();

                return $this->redirect($this->generateUrl('index'));
            }
        }

        return $this->render('@Book/Default/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    // Добавление автора, список авторов, тут ничего интересного
    public function authorAction(Request $request)
    {

        $repo = $this->getDoctrine()->getManager()->getRepository('Vovka\BookBundle\Entity\Author');

        $resource = $repo->findAll();

        $result = array();

        foreach ($resource as $author)
            $result[] = array('id' => $author->getID(), 'name' => $author->getName());

        $author = new Author();

        $form = $this->createFormBuilder($author)
            ->add('name', 'text')
            ->add('save', 'submit', array('label' => 'Добавить'))
            ->getForm();

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $author = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($author);
                $em->flush();

                return $this->redirect($this->generateUrl('author'));
            }
        }

        return $this->render('@Book/Default/author.html.twig', array(
            'authors' => $result,
            'form' => $form->createView(),
        ));
    }

    // Добавление и список издательств, тоже ничего интересного
    public function publishingHouseAction(Request $request)
    {

        $repo = $this->getDoctrine()->getManager()->getRepository('Vovka\BookBundle\Entity\PublishingHouse');

        $resource = $repo->findAll();

        $result = array();

        foreach ($resource as $publishingHouse)
            $result[] = array('id' => $publishingHouse->getID(), 'name' => $publishingHouse->getName());

        $publishingHouse = new PublishingHouse();

        $form = $this->createFormBuilder($publishingHouse)
            ->add('name', 'text')
            ->add('save', 'submit', array('label' => 'Добавить'))
            ->getForm();

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $publishingHouse = $form->getData();

                $em = $this->getDoctrine()->getManager();
                $em->persist($publishingHouse);
                $em->flush();

                return $this->redirect($this->generateUrl('publishingHouse'));
            }
        }

        return $this->render('@Book/Default/publishingHouse.html.twig', array(
            'publishingHouses' => $result,
            'form' => $form->createView(),
        ));
    }

    // Получение значений полей для одной книги, просто для удобства - отдельная функция
    private function extractFieds($book)
    {
        return Array(
            'id' => $book->getId(),
            'name' => $book->getName(),
            'description' => $book->getDescription(),
            'author' => $book->getAuthor()->getName(),
            'dateRelease' => $book->getDateRelease()->format('Y'),
            'publishingHouse' => $book->getPublishingHouse()->getName(),
            'photo' => $book->getWebPath()
        );
    }

}
