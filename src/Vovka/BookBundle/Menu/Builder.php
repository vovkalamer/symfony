<?php

namespace Vovka\BookBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    // Простое меню, ничего лишнего
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Главная', array('route' => 'index'));
        $menu->addChild('Книга+', array('route' => 'new'));
        $menu->addChild('Автор+', array('route' => 'author'));
        $menu->addChild('Издательство+', array('route' => 'publishingHouse'));

        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        return $menu;
    }
}