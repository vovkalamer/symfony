<?php

namespace Vovka\BookBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vovka\BookBundle\Entity\Author;
use Vovka\BookBundle\Entity\PublishingHouse;

class FilterBookType extends AbstractType
{
    protected $name;
    protected $description;
    protected $dateRelease;
    protected $author;
    protected $publishingHouse;
    protected $sortByName;

    public function __construct(array $data = null)
    {
        $this->name = (isset($data['name']) ? $data['name'] : null);
        $this->description = (isset($data['description']) ? $data['description'] : null);
        $this->dateRelease = (isset($data['dateRelease']) ? $data['dateRelease'] : null);
        $this->author = (isset($data['author']) ? $data['author'] : null);
        $this->publishingHouse = (isset($data['publishingHouse']) ? $data['publishingHouse'] : null);
        $this->sortByName = (isset($data['sortByName']) ? $data['sortByName'] : null);
    }

    // Построил кастомную форму для фильтра по книгам
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('required' => false, 'data' => $this->name))
            ->add('description', 'textarea', array(
                'required' => false,
                'data' => $this->description
            ))
            ->add('dateRelease', 'text', array(
                'required' => false,
                'data' => $this->dateRelease
            ))
            ->add('author', 'entity', array(
                'class' => 'Vovka\BookBundle\Entity\Author',
                'property' => 'name',
                'empty_value' => 'Выберите автора',
                'data' => $this->author,
                'required' => false,
            ))
            ->add('publishingHouse', 'entity', array(
                'class' => 'Vovka\BookBundle\Entity\PublishingHouse',
                'property' => 'name',
                'empty_value' => 'Выбрать..',
                'data' => $this->publishingHouse,
                'required' => false,
            ))
            ->add('sortByName', 'choice', array(
                'choices' => array('0' => 'По возрастанию', '1' => 'По убыванию'),
                'data' => $this->sortByName,
                'required' => false,
            ))
            ->add('filter', 'hidden', array('data' => '1'))
            ->add('reset', 'submit', array('label' => 'Сброс'))
            ->add('find', 'submit', array('label' => 'Поиск'))
            ->setMethod('GET')
            ->setAction('/');
    }

    public function getName()
    {
        return 'book';
    }
}