<?php

namespace Vovka\BookBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BookType extends AbstractType
{

    // Кастомная форма для добавления книги
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('description', 'textarea')
            ->add('dateRelease', 'date', array(
                'input' => 'datetime',
                'widget' => 'single_text',
            ))
            ->add('photo', 'file')
            ->add('author', 'entity', array(
                'class' => 'Vovka\BookBundle\Entity\Author',
                'property' => 'name',
            ))
            ->add('publishingHouse', 'entity', array(
                'class' => 'Vovka\BookBundle\Entity\PublishingHouse',
                'property' => 'name',
            ))
            ->add('save', 'submit', array('label' => 'Поехали!'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vovka\BookBundle\Entity\Book',
        ));
    }

    public function getName()
    {
        return 'book';
    }
}