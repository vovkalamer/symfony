<?php

namespace Vovka\BookBundle\Repository;

use Doctrine\ORM\EntityRepository;

class BookRepository extends EntityRepository
{
    // фильтрация по полям книги
    public function findByParams(array $filter)
    {
        $query = $this->createQueryBuilder('b');

        $andX = $query->expr()->andX(
            $query->expr()->like('b.name', ':name'),
            $query->expr()->like('b.description', ':description'),
            $query->expr()->like('b.dateRelease', ':dateRelease')
        );

        $query
            ->setParameter('name', '%' . $filter['name'] . '%')
            ->setParameter('description', '%' . $filter['description'] . '%')
            ->setParameter('dateRelease', '%' . $filter['dateRelease'] . '%');

        if (intval($filter['author']) > 0) {
            $andX->add($query->expr()->eq('b.author', ':author'));
            $query->setParameter('author', intval($filter['author']));
        }

        if (intval($filter['publishingHouse']) > 0) {
            $andX->add($query->expr()->eq('b.publishingHouse', ':publishingHouse'));
            $query->setParameter('publishingHouse', intval($filter['publishingHouse']));
        }

        $query
            ->add('where', $andX);

        if (isset($filter['sortByName']))
            switch ($filter['sortByName']) {
                case '0':
                    $query->addOrderBy('b.name');
                    break;
                case '1':
                    $query->addOrderBy('b.name', 'DESC');
                    break;
            }

        return $query->getQuery()
            ->getResult();
    }
}