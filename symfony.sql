-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Мар 15 2015 г., 16:55
-- Версия сервера: 5.5.41-0ubuntu0.14.04.1
-- Версия PHP: 5.5.22-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `symfony`
--

-- --------------------------------------------------------

--
-- Структура таблицы `authors`
--

CREATE TABLE IF NOT EXISTS `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `authors`
--

INSERT INTO `authors` (`id`, `name`) VALUES
(1, 'Пушкин'),
(2, 'Гоголь'),
(3, 'Толстой'),
(4, '123'),
(5, '12'),
(6, '123'),
(7, '1'),
(8, '2'),
(9, '3'),
(10, '123'),
(11, '321');

-- --------------------------------------------------------

--
-- Структура таблицы `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `author` int(11) DEFAULT NULL,
  `dateRelease` date NOT NULL,
  `publishingHouse` int(11) DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_2` (`id`),
  KEY `IDX_4A1B2A92BDAFD8C8` (`author`),
  KEY `IDX_4A1B2A924C02AD2B` (`publishingHouse`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Дамп данных таблицы `books`
--

INSERT INTO `books` (`id`, `name`, `description`, `author`, `dateRelease`, `publishingHouse`, `path`) VALUES
(1, 'Война и Мир', 'Много томов', 3, '1869-01-01', 1, NULL),
(12, '123', '123', 1, '2010-01-01', 1, 'Снимок экрана от 2015-03-10 22:45:57.png'),
(13, 'Тестовая книга', 'И тут короче такое описание', 3, '2015-01-01', 2, 'Снимок экрана от 2015-03-10 22:45:57.png'),
(14, 'Тестовая книга', '123', 2, '2010-01-01', 1, 'Снимок экрана от 2015-03-07 19:39:06.png'),
(15, '123123', '123123', 6, '2010-01-01', 5, 'Снимок экрана от 2015-03-09 11:44:45.png'),
(16, 'Последний тест', 'Самый последний, правда', 1, '2015-03-15', 1, 'Снимок экрана от 2015-03-09 11:44:45.png');

-- --------------------------------------------------------

--
-- Структура таблицы `publishingHouse`
--

CREATE TABLE IF NOT EXISTS `publishingHouse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `publishingHouse`
--

INSERT INTO `publishingHouse` (`id`, `name`) VALUES
(1, 'Дрофа'),
(2, 'Тест'),
(3, '123'),
(4, '312312'),
(5, 'Тест');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `FK_4A1B2A924C02AD2B` FOREIGN KEY (`publishingHouse`) REFERENCES `publishingHouse` (`id`),
  ADD CONSTRAINT `FK_4A1B2A92BDAFD8C8` FOREIGN KEY (`author`) REFERENCES `authors` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
